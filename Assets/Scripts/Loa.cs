using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loa : MonoBehaviour
{
    public void Load()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("SampleScene");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public TextMeshProUGUI scoreText;

    private void Start()
    {
        scoreText.text = PlayerPrefs.GetInt("score").ToString();
    }
}
