using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answers : MonoBehaviour
{
    public bool isCorrect = false;
    public QuizManager manager;
    
    
    public void Answer()
    {
        if (isCorrect)
        {
            Debug.Log("Correct Answer");
            manager.Correct();
        }
        else
        {
            Debug.Log("Wrong Answer");
            manager.NotCorrect();
        }
    }
}
