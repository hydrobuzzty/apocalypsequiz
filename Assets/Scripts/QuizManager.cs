using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class QuizManager : MonoBehaviour
{
    public List<QandA> qa;
    public GameObject[] options;
    public int currentQuestion;
    public int score = 0;

    public TextMeshProUGUI questionText;

    private void Start()
    {
        GenerateQuestions();
    }

    public void Correct()
    {
        score++;
        qa.RemoveAt(currentQuestion);
        GenerateQuestions();
    }
    public void NotCorrect()
    {
        qa.RemoveAt(currentQuestion);
        GenerateQuestions();
    }

    private void SetAnswers()
    {
        for (int i = 0; i < options.Length; i++)
        {
            options[i].GetComponent<Answers>().isCorrect = false;
            options[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = qa[currentQuestion].answers[i];

            if (qa[currentQuestion].correctAnswer == i+1)
            {
                options[i].GetComponent<Answers>().isCorrect = true;
            }
        }
    }

    private void GenerateQuestions()
    {
        if (qa.Count > 0)
        {
            currentQuestion = Random.Range(0, qa.Count);

            questionText.text = qa[currentQuestion].question;
            SetAnswers();
        }
        else
        {
            PlayerPrefs.SetInt("score", score);
            SceneManager.LoadScene("End");
        }
    }
}
